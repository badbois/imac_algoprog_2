#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;


void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
    int start=0, end=array.size();
    int mid;
    indexMax=-1;
    indexMin=-1;
    //On cherche l'index min
    while(start<end){
        if(start==(end-1)){
            indexMin=end;
            break;
        }else{
            mid=(start+end)/2;
            if(array[mid]<toSearch){
                start=mid;

            }else if(array[mid]>=toSearch){
                end=mid;
            }
        }
    }

    start=0;
    end=array.size();
    //On cherche l'index max
    while(start<end){
        if(start==(end-1)){
            indexMax=start;
            break;
        }else{
            mid=(start+end)/2;
            if(array[mid]<=toSearch){
                start=mid;

            }else if(array[mid]>toSearch){
                end=mid;
            }
        }
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
