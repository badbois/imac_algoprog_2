#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
    bool trie;
    for(int i=0;i<toSort.size()-1;i++){
        trie=true;
        for(int j=0; j<toSort.size()-1 ;j++){
            if(toSort.get(j)>toSort.get(j+1)){
                toSort.swap(j,j+1);
                trie=false;
            }
        }
        if(trie){
            break;
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
