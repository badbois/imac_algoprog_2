#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());
    sorted.set(0,toSort.get(0));
    int taille=1;
    bool trie;
    for (int i=1;i<toSort.size();i++) {
        trie=true;
        for (int j=0;j<taille && trie==true ;j++) {
            if(sorted.get(j)>=toSort.get(i)){
                sorted.insert(j,toSort.get(i));
                trie=false;
            }
        }
        if(trie==true){
            sorted.insert(taille,toSort.get(i));
        }
        taille++;
    }
	
    toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
