#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
    if(origin.size()==1){
        return;
    }

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    for(int i=0; i<origin.size(); i++){
        if(i<first.size()){
            first[i]=origin[i];
        }else {
            second[i-first.size()]=origin[i];
        }
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);

	// merge
    merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
    int indexF=0, indexS=0, compteur=0;
    while(compteur<first.size()+second.size()){
        if(indexF==first.size()){
            result[compteur]=second[indexS];
            indexS++;
            compteur++;
        }else if(indexS==second.size()){
            result[compteur]=first[indexF];
            indexF++;
            compteur++;
        }else if(first[indexF]<second[indexS]){
            result[compteur]=first[indexF];
            indexF++;
            compteur++;
        }else{
            result[compteur]=second[indexS];
            indexS++;
            compteur++;
        }
    }
}




void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
