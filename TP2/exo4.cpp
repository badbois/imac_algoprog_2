#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
    if(toSort.size()==1){
        return;
    }
	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0; // effectives sizes
    int pivot= toSort[0];
    for (int i=1; i< toSort.size(); i++){
        if(i<toSort[pivot]){
            lowerArray.set(lowerSize, toSort[i]);
            lowerSize++;
        }
        else if(i>toSort[pivot]){
            greaterArray.set(greaterSize, toSort[i]);
            greaterSize++;
        }
    }
	// recursiv sort of lowerArray and greaterArray
    recursivQuickSort(lowerArray, lowerSize);
    recursivQuickSort(greaterArray,greaterSize);

	// merge
    for (int i=0; i< toSort.size(); i++){
        if(i<lowerSize && i<pivot){
            toSort[i]=lowerArray[i];
        }
        else if(i>pivot){
            toSort[i]=greaterArray[i];
        }
        else{
            toSort[i]=pivot;
        }
    }
}

void quickSort(Array& toSort){
    recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
