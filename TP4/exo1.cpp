#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    this->get(i)=value;
    while(i>0 && this->get(i)>this->get((i-1)/2)){
        this->swap(i, (i-1)/2);
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
        //On regarde si les deux fils sont dans le tableau
     if(this->rightChild(i_max) < heapSize && this->leftChild(i_max) < heapSize){
            //Si les deux fils sont plus grand on choisis le plus grand des deux
         if(this->get(i_max)< this->get(this->leftChild(i_max)) || this->get(i_max)< this->get(this->rightChild(i_max))){
             if(this->get(this->leftChild(i_max)) < this->get(this->rightChild(i_max))){
                 this->swap(i_max, this->rightChild(i_max));
                 heapify(heapSize, this->rightChild(i_max));
             }else{
                 this->swap(i_max, this->leftChild(i_max));
                 heapify(heapSize, this->leftChild(i_max));
             }
         }
         //Sinon Si un seul fils est dans le tableau, on regarde s'il est plus grand que son parent
     }else if(this->rightChild(i_max) < heapSize && this->leftChild(i_max) >= heapSize){
          if(this->get(i_max)< this->get(this->rightChild(i_max))){
              this->swap(i_max, this->rightChild(i_max));
              heapify(heapSize, this->rightChild(i_max));
          }
     }else if(this->rightChild(i_max) >= heapSize && this->leftChild(i_max) < heapSize){
         if(this->get(i_max)< this->get(this->leftChild(i_max))){
             this->swap(i_max, this->leftChild(i_max));
             heapify(heapSize, this->leftChild(i_max));
         }
     }
}

void Heap::buildHeap(Array& numbers)
{
    for(int i=0; i<numbers.size(); i++){
        insertHeapNode(i+1, numbers[i]);
    }

}

void Heap::heapSort()
{
    for(int i=this->size()-1; i>=0; i--){
        this->swap(0,i);
        this->heapify(i, 0);
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
