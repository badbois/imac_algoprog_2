#include "tp1.h"
#include <QApplication>
#include <time.h>

void CalculPointApres(Point *z, Point point){
    float re=z->x;
    float im=z->y;
    z->x=(re*re)-(im*im) + point.x;
    z->y=2*re*im + point.y;
}

int isMandelbrot(Point z, int n, Point point){
    CalculPointApres(&z, point);
    float module=sqrt((z.x)*(z.x)+(z.y)*(z.y));
    if(module > 2){
        return n;
    }else if(n==0){
        return 0;
    }else{
        return isMandelbrot(z, n-1, point);
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



